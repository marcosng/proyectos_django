from django.contrib import admin
from .forms import TareaForm, BogusExportForm
from django.forms import TextInput, Textarea
from .actions import *
from django.shortcuts import render
from import_export.admin import ImportExportModelAdmin
from .models import Sitio
from import_export import resources, fields


#@admin.register(Gerencia)
class GerenciaAdmin(admin.ModelAdmin):
    model = Gerencia
    list_display = ('area',)


class ProvinciaAdmin(admin.ModelAdmin):
    model = Provincia
    list_display = ('prov',)


# Clase Resource para IMPORT/EXPORT
class SitioResource(resources.ModelResource):
    cellid = fields.Field(column_name='CellID', attribute='cellid')
    nombre = fields.Field(column_name='Nombre', attribute='nombre')
    provincia = fields.Field(column_name='Provincia', attribute='provincia__nombre')
    zona = fields.Field(column_name='Zona', attribute='zona')
    gerencia = fields.Field(column_name='Gerencia', attribute='gerencia__area')
    coordenadas = fields.Field(column_name='Lat/Long', attribute='coordenadas')

    class Meta:
        model = Sitio
        fields = ['cellid', 'nombre', 'provincia', 'zona', 'gerencia', 'coordanadas']
        export_order = ['cellid', 'nombre', 'provincia', 'zona', 'gerencia', 'coordenadas']
        import_id_fields = ['cellid']
        exclude = ['id']
@admin.register(Sitio)
class SitioAdmin(ImportExportModelAdmin):
    fields = [
        ('cellid', 'nombre'),
        ('provincia', 'zona', 'gerencia'),
        'domicilio',
        'coordenadas'
            ]
    list_display = ('cellid', 'nombre', 'provincia', 'domicilio')
    search_fields = ('cellid', 'nombre', 'provincia')
    resource_class = SitioResource


class PepAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'monto')


class PepInline(admin.StackedInline):
    model = Pep
    list_display = ('nombre', 'monto')
    extra = 1


@admin.register(LineaCapex)
class LineaCapexAdmin(admin.ModelAdmin):
    model = LineaCapex
    list_display = ('linea_capex', 'descripcion', )
    inlines = [PepInline]
    actions = ['reporte_lineacapex']

    def reporte_lineacapex(self, request, queryset):
        dic_lc = {}
        for lc in queryset:
            dic_ = {}
            dic_peps = {}
            val = 0
            for pep in lc.pep_set.all():
                dic_peps[pep.nombre] = pep.monto.amount
                val += pep.monto.amount
            dic_["total"] = val
            dic_["peps"] = dic_peps
            dic_lc[lc.linea_capex] = dic_

        # form = BogusExportForm(initial={'_selected_action': query.ids})
        # context = dict(make_context(board, request, query.name), action=get_action(request), form=form, params=None)
        # ret = render(request, 'dashboard/table_action.html', context)
        #
        # return ret

        return render(request, 'manager/list_table_lineacapex.html', {'LineaCapex': dic_lc,
                                                                       'opts': self.model._meta,
                                                                       'app_label': self.model._meta.app_label,
                                                                       'title': 'Reporte de Montos',
                                                                       'has_permission': True})
    reporte_lineacapex.short_description = 'Reporte de Montos'




class TipoContratoAdmin(admin.ModelAdmin):
    model = TipoContrato
    list_display = ('contrato', )


class EstadoAdmin(admin.ModelAdmin):
    list_display = ('estado', )


class RubroAdmin(admin.ModelAdmin):
    list_display = ('rubro', )


class ContratistaResource(resources.ModelResource):
    nombre = fields.Field(column_name='Nombre', attribute='nombre')
    identi = fields.Field(column_name='ID', attribute='identi')
    domicilio = fields.Field(column_name='Domicilio', attribute='domicilio')
    telefono = fields.Field(column_name='Telefono', attribute='telefono')
    email = fields.Field(column_name='Email', attribute='email')
    class Meta:
        model = Contratista
        fields = ['nombre', 'identi', 'domicilio', 'telefono', 'email']
        export_order = ['nombre', 'identi', 'domicilio', 'telefono', 'email']
        import_id_fields = ['nombre']
        exclude = ['id']
@admin .register(Contratista)
class ContratistaAdmin(ImportExportModelAdmin):
    list_display = ('nombre', 'identi', 'domicilio', 'telefono', 'email')
    actions = ['ver_contratista']
    resource_class = ContratistaResource

    def ver_contratista(self, request, queryset):
        return render(request, 'manager/list_table_contratista.html', {'Contratista': queryset.reverse(),
                                                                       'opts': self.model._meta,
                                                                       'app_label': self.model._meta.app_label,
                                                                       'title': 'Lista de Contratistas',
                                                                       'has_permission': True})
    ver_contratista.short_description = 'Ver contratistas...'


class ResponsableResource(resources.ModelResource):
    nombre = fields.Field(column_name='Nombre', attribute='nombre')
    apellido = fields.Field(column_name='Apellido', attribute='apellido')
    legajo = fields.Field(column_name='Legajo', attribute='legajo')
    class Meta:
        model = Responsable
        fields = ['nombre', 'apellido', 'legajo']
        export_order = ['nombre', 'apellido', 'legajo']
        import_id_fields = ['nombre', 'apellido']
        exclude = ['id']
@admin.register(Responsable)
class ResponsableAdmin(ImportExportModelAdmin):
    list_display = ('nombre_completo', 'legajo')
    unique_together = ('nombre', 'apellido')
    resource_class = ResponsableResource


class CertificacionInline(admin.StackedInline):
    model = Certificacion
    list_display = ('cert_num', 'fecha', 'recepcion', 'monto', 'cambio', 'dolares', )
    readonly_fields = ('cert_num', 'dolares', )
    extra = 1


@admin.register(Tarea)
class TareaAdmin(admin.ModelAdmin):

    form = TareaForm

    # AJUSTAR EL TAMAÑO DE LOS CHARFIELD y TEXTFIELD
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '10'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 2, 'cols': 30})},
    }

    # SEPARO LA ENTRADA DE LOS DATOS EN CATEGORIAS
    fieldsets = (
        (None, {
            'fields': ('tarea_num',
                       ('ticket', 'responsable', 'contratista'),
                       ('cellid', 'nombre_sitio'),
                       ('rubro', 'estado', 'contr'),
                       ('fecha_pedido', 'semana_pedido', 'anio_pedido'),
                       ('fecha_compromiso', 'semana_compromiso', 'anio_compromiso'),
                       ('fecha_finaliz', 'semana_finaliz', 'anio_finaliz'),
                       ('descripcion', 'detalle', 'cao')
                       )
        }),
        ('Orden de Compra', {
            'fields': (('oc', 'tipo_prov'),
                       'oc_monto_est',
                       'oc_monto',
                       'oc_pep'
                       )
        })
    )

    readonly_fields = ('tarea_num', )
    list_display = ('ticket', 'cellid', 'responsable', 'contratista', 'estado', 'oc_monto', 'monto_certif', 'porcent_compl')
    search_fields = ('ticket', 'cellid__cellid', 'contratista__nombre', 'responsable__apellido')
    inlines = [CertificacionInline]
    actions = [reporte_tareas, reporte_montos_por_rubro, reporte_montos_por_hos]

@admin.register(Certificacion)
class CertificacionAdmin(admin.ModelAdmin):
    list_display = ['tarea_ticket', 'tarea_cellid', 'fecha', 'recepcion', 'monto', 'cambio', 'dolares']
    readonly_fields = ('cert_num', 'dolares', )

    # COMO MOSTRAR FOREIGNKEY EN LIST_DISPLAY
    # TODO: Mostrar ForeigKey en List_display
    def tarea_ticket(self, obj):
        return obj.tarea.ticket
    tarea_ticket.short_description = 'Ticket Remedy'

    def tarea_cellid(self, obj):
        return obj.tarea.cellid
    tarea_cellid.short_description = 'Sitio'
