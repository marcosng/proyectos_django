from SegProyectoInfra.Manager.models import *
from django.http import HttpResponse
import csv


# ADMIN ACTIONS: https://simpleisbetterthancomplex.com/tutorial/2017/03/14/how-to-create-django-admin-list-actions.html
# =============
def reporte_tareas(self, request, queryset):
    # # Mensaje que se muestra finalizada la accion
    # message_bit = 'Se exportaron {0} tareas!'.format(len(queryset) )
    # self.message_user(request, message_bit)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tarea.csv"'
    writer = csv.writer(response)
    writer.writerow(['ID', 'Ticket Remedy', 'Sitio', 'Estado', 'Cant. Certificaciones'])
    tareas = queryset.values_list('tarea_num', 'ticket', 'cellid__cellid', 'estado__estado')

    for tarea in tareas:
        # Añado la cantidad de certificacion por tarea a la exportacion
        cuenta = Certificacion.objects.all().filter(tarea__tarea_num=str(tarea[0])).count()
        tarea2 = list(tarea)
        tarea2.append(cuenta)
        writer.writerow(tarea2)
    return response
reporte_tareas.short_description = 'Exportar tareas'


def reporte_montos_por_rubro(self, request, queryset):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Reporte_montos_por_rubro.csv"'
    writer = csv.writer(response)
    writer.writerow(['Rubro', 'Tareas', 'Monto OC', 'Monto Certif'])

    rubros = [n.rubro for n in Rubro.objects.all()]
    for rub in rubros:
        tareas = queryset.filter(rubro__rubro=rub)
        aux1 = str()
        aux2 = 0
        aux3 = 0
        for item in tareas:
            # Tareas pertenecientes al mismo rubro
            aux1 = item.tarea_num + ';' + aux1

            # Si no se cargo monto en la OC
            if item.oc_monto is not None:
                # Monto de OC de las tareas pertenecientes al mismo rubro
                aux2 = aux2 + item.oc_monto.amount

                # Monto de todas las certificaciones de las tarea pertenecientes al mismo rubro
                cert = Certificacion.objects.all().filter(tarea__tarea_num=item)
                for item2 in cert:
                    aux3 = item2.monto.amount + aux3

        aux1 = aux1[:len(aux1) - 1]

        lista = [rub, aux1, aux2, aux3]
        writer.writerow(lista)
    return response
reporte_montos_por_rubro.short_description = 'Report de montos por rubro'


def reporte_montos_por_hos(self, request, queryset):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Reporte_montos_por_HoS.csv"'
    writer = csv.writer(response)
    writer.writerow(['HoS', 'Tareas', 'Monto OC', 'Monto Certif'])

    dic_hos = {'1': 'Servicio', '2': 'Hardware'}
    for hos in ['1', '2']:
        tareas = queryset.filter(tipo_prov=hos)
        aux1 = str()
        aux2 = 0
        aux3 = 0
        for item in tareas:
            # Tareas pertenecientes al mismo rubro
            aux1 = item.tarea_num + ';' + aux1

            # Monto de OC de las tareas pertenecientes al mismo rubro
            aux2 = aux2 + item.oc_monto.amount

            # Monto de todas las certificaciones de las tarea pertenecientes al mismo rubro
            cert = Certificacion.objects.all().filter(tarea__tarea_num=item)
            for item2 in cert:
                aux3 = item2.monto.amount + aux3

        aux1 = aux1[:len(aux1) - 1]

        lista = [dic_hos[hos], aux1, aux2, aux3]
        print(lista)
        writer.writerow(lista)
    return response
reporte_montos_por_hos.short_description = 'Report de montos por HoS'
