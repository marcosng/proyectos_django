from django import forms
from SegProyectoInfra.Manager.models import Tarea, LineaCapex


class TareaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TareaForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, 'cellid'):
            self.fields['nombre_sitio'].initial = self.instance.cellid

        self.fields['semana_pedido'].initial = self.instance.semana_pedido
        self.fields['anio_pedido'].initial = self.instance.anio_pedido

        self.fields['semana_compromiso'].initial = self.instance.semana_compromiso
        self.fields['anio_compromiso'].initial = self.instance.anio_compromiso

        self.fields['semana_finaliz'].initial = self.instance.semana_finaliz
        self.fields['anio_finaliz'].initial = self.instance.anio_finaliz

        self.fields['monto_certif'].initial = self.instance.monto_certif

        self.fields['porcent_compl'].initial = self.instance.porcent_compl

    nombre_sitio = forms.CharField(label='Nombre del sitio', disabled=True, required=False)

    semana_pedido = forms.CharField(label='Semana de Pedido', disabled=True, required=False)
    anio_pedido = forms.CharField(label='Año de Pedido', disabled=True, required=False)

    semana_compromiso = forms.CharField(label='Semana de Compromiso', disabled=True, required=False)
    anio_compromiso = forms.CharField(label='Año de Compromiso', disabled=True, required=False)

    semana_finaliz = forms.CharField(label='Semana de Finalizacion', disabled=True, required=False)
    anio_finaliz = forms.CharField(label='Año de Finalizacion', disabled=True, required=False)

    monto_certif = forms.CharField(label='Monto Certificado', disabled=True, required=False)

    porcent_compl = forms.CharField(label='Porcent. Completado', disabled=True, required=False)


    class Meta(forms.ModelForm):
        fields = '__all__'
        model = Tarea

class LineaCapexForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(LineaCapexForm, self).__init__(*args, **kwargs)

        self.fields['monto_total'].initial = self.instance.monto_total
        self.fields['monto_disp'].initial = self.instance.monto_disp

    monto_total = forms.IntegerField(label='Monto Total', disabled=True, required=False, help_text='Monto total de la línea de CAPEX')
    monto_disp = forms.IntegerField(label='Monto Disp.', disabled=True, required=False,help_text='Monto remanente disponible de la línea de CAPEX')

    class Meta(forms.ModelForm):
        fields = '__all__'
        model = LineaCapex

class BogusExportForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)