from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import transaction
from geoposition.fields import GeopositionField
from djmoney.models.fields import MoneyField
# TODO: marcosg/nicolas123 - marcosg2/nicolas123


# PARAMETOS GLOBALES
L_LENGTH = 60
M_LENGTH = 30
S_LENGTH = 10


class Gerencia(models.Model):
    area = models.CharField(verbose_name='Gerencia Operaciones', max_length=M_LENGTH, blank=False, unique=True)

    def __str__(self):
        return self.area


class Provincia(models.Model):
    nombre = models.CharField(verbose_name='Provincia', max_length=L_LENGTH, blank=False)

    def __str__(self):
        return self.nombre


class Sitio(models.Model):
    cellid = models.CharField(verbose_name='Cell ID', max_length=S_LENGTH, blank=False, unique=True)
    nombre = models.CharField(verbose_name='Nombre', max_length=M_LENGTH, blank=False, unique=True)
    domicilio = models.CharField(verbose_name='Direccion', max_length=M_LENGTH, blank=True)
    provincia = models.ForeignKey(Provincia, verbose_name='Provincia', max_length=M_LENGTH, on_delete=models.CASCADE, blank=True, null=True)

    ZONA = (('INTERIOR', 'INTERIOR'), ('AMBA', 'AMBA'))
    zona = models.CharField(verbose_name='Zona', max_length=M_LENGTH, blank=True, choices=ZONA)
    gerencia = models.ForeignKey(Gerencia, verbose_name='Gerencia', max_length=L_LENGTH, on_delete=models.CASCADE, blank=True, null=True)
    coordenadas = GeopositionField(verbose_name='Lat./Lon.', blank=True, null=True)

    def __str__(self):
        return self.cellid


class LineaCapex(models.Model):
    linea_capex = models.CharField(verbose_name='Línea Capex', max_length=S_LENGTH, blank=False)
    descripcion = models.CharField(verbose_name='Descripcion', max_length=L_LENGTH, blank=False)

    # @property
    # def monto_total(self):
    #     items = Pep.objects.all().filter(lcapex__linea_capex=self.linea_capex)
    #     val = 0
    #     for item in items:
    #         val = val + item.monto.amount
    #     return val
    #
    # @property
    # def monto_disp(self):
    #     items = Tarea.objects.all().filter(oc_pep__nombre=self.linea_capex)
    #     val = 0
    #     for item in items:
    #         val = val + item.oc_monto.amount
    #     return self.monto_total - val

    class Meta:
        verbose_name_plural = 'Línea CAPEX'

    def __str__(self):
        return self.linea_capex


class Pep(models.Model):
    class Meta:
        verbose_name_plural = 'PePs'
    nombre = models.CharField(verbose_name='PeP', max_length=L_LENGTH, blank=False)
    comentario = models.CharField(verbose_name='Comentario', max_length=L_LENGTH, blank=True)
    monto = MoneyField(max_digits=12, decimal_places=2, default_currency='ARS', blank=False)
    lcapex = models.ForeignKey(LineaCapex, verbose_name='Linea Capex', blank=False, on_delete=models.CASCADE)

    def __str__(self):
        string = '{} - {}'.format(self.lcapex, self.nombre,)
        return string


class Responsable(models.Model):
    nombre = models.CharField(verbose_name='Nombre', max_length=M_LENGTH, blank=False)
    apellido = models.CharField(verbose_name='Apellido', max_length=M_LENGTH, blank=False)
    legajo = models.CharField(verbose_name='Legajo', max_length=S_LENGTH, blank=False)

    def nombre_completo(self):
        return '{0}, {1}'.format(self.apellido, self.nombre)

    def __str__(self):
        return '{0}, {1}'.format(self.apellido, self.nombre)


class Contratista(models.Model):
    nombre = models.CharField(verbose_name='Nombre', max_length=L_LENGTH, blank=False, unique=True)
    identi = models.CharField(verbose_name='ID', max_length=M_LENGTH, blank=False, unique=True)
    domicilio = models.CharField(verbose_name='Direccion', max_length=L_LENGTH, blank=True)
    telefono = models.CharField(verbose_name='Telefono', max_length=M_LENGTH, blank=True)
    email = models.EmailField(verbose_name='Email', max_length=M_LENGTH, blank=True)

    def __str__(self):
        return self.nombre


class Estado(models.Model):
    estado = models.CharField(verbose_name='Estado de tarea', max_length=M_LENGTH, blank=False, unique=True)

    def __str__(self):
        return self.estado


class Rubro(models.Model):
    rubro = models.CharField(verbose_name='Rubro de tarea', max_length=M_LENGTH, blank=False, unique=True)

    def __str__(self):
        return self.rubro


class TipoContrato(models.Model):
    contrato = models.CharField(verbose_name='Tipo de Contrato', max_length=M_LENGTH, blank=False, unique=True)

    def __str__(self):
        return self.contrato


class Tarea(models.Model):
    tarea_num = models.CharField(verbose_name='ID', blank=False, null=True, max_length=M_LENGTH)

    ticket = models.CharField(verbose_name='Ticket Remedy', max_length=M_LENGTH, unique=True, blank=True, null=True)

    cellid = models.ForeignKey(Sitio, verbose_name='Sitio asociado', max_length=M_LENGTH, on_delete=models.SET_NULL, blank=True, null=True)
    contratista = models.ForeignKey(Contratista, verbose_name='Contratista', max_length=M_LENGTH, on_delete=models.SET_NULL, blank=True, null=True)
    responsable = models.ForeignKey(Responsable, verbose_name='Responsable', max_length=M_LENGTH, on_delete=models.SET_NULL, blank=True, null=True)
    estado = models.ForeignKey(Estado, verbose_name='Estado', max_length=M_LENGTH, on_delete=models.SET_NULL, blank=True, null=True)
    rubro = models.ForeignKey(Rubro, verbose_name='Rubro', max_length=M_LENGTH, on_delete=models.SET_NULL, blank=True, null=True)
    contr = models.ForeignKey(TipoContrato, verbose_name='Tipo de Contrato', max_length=M_LENGTH, on_delete=models.SET_NULL, blank=True, null=True)

    # ÓRDEN DE COMPRA Y CAPEX
    oc_monto_est = models.IntegerField(verbose_name='Monto OC Estimado', help_text='En pesos (ARS)', blank=False, null=True)
    oc = models.CharField(verbose_name='OC#', max_length=15, blank=True)
    oc_monto = MoneyField(verbose_name='Monto OC', max_digits=S_LENGTH, decimal_places=2, default_currency='ARS', blank=True, null=True)

    oc_pep = models.ForeignKey(Pep, verbose_name='Línea Capex - PeP', max_length=M_LENGTH, on_delete=models.SET_NULL, blank=True, null=True)

    TIPO = (('1', 'Servicios'), ('2', 'Hardware'))
    tipo_prov = models.CharField(verbose_name='Tipo de Provisión', max_length=M_LENGTH, choices=TIPO, blank=True, null=True)

    # DESCRIPCION Y FECHAS
    descripcion = models.TextField(verbose_name='Descripcion',blank=True, null=True)
    fecha_pedido = models.DateField(verbose_name='Fecha de Pedido', blank=False, null=True)
    fecha_compromiso = models.DateField(verbose_name='Fecha de Compromiso', blank=True, null=True)
    fecha_finaliz = models.DateField(verbose_name='Fecha de Finalizacion', blank=True, null=True)

    cao = models.BooleanField(verbose_name='CAO', default=False)
    detalle = models.TextField(verbose_name='Detalle', blank=True, null=True)

    @property
    def nombre_sitio(self):
        print(self.cellid__nombre)
        return self.cellid

    @property
    def semana_pedido(self):
        return self.fecha_pedido.isocalendar()[1] if self.fecha_pedido is not None else None

    @property
    def anio_pedido(self):
        return self.fecha_pedido.year if self.fecha_pedido is not None else None

    @property
    def semana_compromiso(self):
        return self.fecha_compromiso.isocalendar()[1] if self.fecha_compromiso is not None else None

    @property
    def anio_compromiso(self):
        return self.fecha_compromiso.year if self.fecha_compromiso is not None else None

    @property
    def semana_finaliz(self):
        return self.fecha_finaliz.isocalendar()[1] if self.fecha_finaliz is not None else None

    @property
    def anio_finaliz(self):
        return self.fecha_finaliz.year if self.fecha_finaliz is not None else None

    @property
    def monto_certif(self):
        cert = Certificacion.objects.all().filter(tarea__ticket=self.ticket)
        m = 0
        for c in cert:
            m = c.monto.amount + m
        return 'ARS$'+str(m)

    @property
    def porcent_compl(self):
        if self.oc_monto is not None:
            cert = Certificacion.objects.all().filter(tarea__ticket=self.ticket)
            if len(cert) != 0:
                m = 0
                for c in cert:
                    m = c.monto.amount + m
                p = "{0:.1f}%".format( 100*m/self.oc_monto.amount )

            else:
                p = '-'
        else:
            p = '-'
        return p

    def __str__(self):
        return str(self.tarea_num)


class Certificacion(models.Model):
    class Meta:
        verbose_name_plural = 'Certificaciones'

    cert_num = models.CharField(verbose_name='ID', max_length=M_LENGTH, blank=False)
    tarea = models.ForeignKey(Tarea, verbose_name='Tarea', blank=False, max_length=M_LENGTH, on_delete=models.CASCADE)

    fecha = models.DateField(verbose_name='Fecha de Certificacion', blank=False, null=True)
    monto = MoneyField(verbose_name='Monto Certificacion', max_digits=12, decimal_places=2, default_currency='ARS',
                       blank=True, null=True)

    cambio = models.FloatField(verbose_name='Tipo de Cambio', blank=True, null=True)
    recepcion = models.CharField(verbose_name='Nro. Recepcion', max_length=M_LENGTH, blank=True)

    dolares = MoneyField(verbose_name='Monto en US$', max_digits=20, decimal_places=2, default_currency='USD',
                         blank=True)

    def save(self):
        # Si la moneda es ARS, al monto en dolares lo calculo segun el tipo de cambio
        if str(self.monto.currency) == 'ARS':
            self.dolares.amount = self.cambio * float(self.monto.amount)
        else:
            self.dolares.amount = self.monto.amount
        super(Certificacion, self).save()

    def __str__(self):
        return self.cert_num


# SEÑALES POST_SAVE
@transaction.atomic
@receiver(post_save, sender=Tarea)
def generar_tarea_num(sender, instance, **kwargs):
    if kwargs['created'] is True:
        string = 'T#{:04d}'.format(Tarea.objects.all().count())
        instance.tarea_num = string
        post_save.disconnect(generar_tarea_num, sender=Tarea)
        instance.save()
        post_save.connect(generar_tarea_num, sender=Tarea)


@transaction.atomic
@receiver(post_save, sender=Certificacion)
def generar_cert_num(sender, instance, **kwargs):
    if kwargs['created'] is True:
        tar = instance.tarea
        obj_count = Certificacion.objects.all().filter(tarea=tar).count()
        string = 'C#{:03d}-{}'.format(obj_count, tar)
        instance.cert_num = string

        post_save.disconnect(generar_cert_num, sender=Certificacion)
        instance.save()
        post_save.connect(generar_cert_num, sender=Certificacion)
