@echo off
title Carga de datos iniciales a modelos
echo -Activando entorno virtual...
cd..
cd venv
cd Scripts
call activate
echo -Cargando datos a modelos...
cd../..
cd SegProyectoInfra
python manage.py loaddata initial_contratistas.json
python manage.py loaddata initial_estado.json
python manage.py loaddata initial_gerencia.json
python manage.py loaddata initial_provincia.json
python manage.py loaddata initial_responsable.json
python manage.py loaddata initial_rubro.json
python manage.py loaddata initial_tipocontrato.json
echo -Carga finalizada!
pause